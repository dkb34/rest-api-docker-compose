from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from sqlalchemy import create_engine, Column, Integer, Float, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from datetime import datetime

app = FastAPI()

DATABASE_URL = "postgresql://user:password@postgres:5432/co2db"
engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()

class CO2Measurement(Base):
    __tablename__ = "measurements"
    id = Column(Integer, primary_key=True, index=True)
    co2 = Column(Float, nullable=False)
    timestamp = Column(DateTime, default=datetime.utcnow)

Base.metadata.create_all(bind=engine)

class Measurement(BaseModel):
    co2: float

@app.post("/measurements/")
def create_measurement(measurement: Measurement):
    db = SessionLocal()
    db_measurement = CO2Measurement(co2=measurement.co2)
    db.add(db_measurement)
    db.commit()
    db.refresh(db_measurement)
    db.close()
    return db_measurement

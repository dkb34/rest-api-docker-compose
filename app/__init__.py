version: '3.8'
services:
  fastapi:
    build: .
    ports:
      - "80:80"
    depends_on:
      - postgres
    networks:
      - mynetwork

  postgres:
    image: 'postgres'
    environment:
      POSTGRES_DB: mydatabase
      POSTGRES_USER: myuser
      POSTGRES_PASSWORD: mypassword
    networks:
      - mynetwork

networks:
  mynetwork:
